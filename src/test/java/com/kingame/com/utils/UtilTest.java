package com.kingame.com.utils;

import com.king.game.utils.Util;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.assertTrue;

import java.io.InputStream;
import java.util.Properties;

/**
 * @author Raul Klumpp - raulklumpp@gmail.com
 */
public class UtilTest {

    Util util;

    private Properties prop;

    InputStream resourceAsStream;

    @Before
    public void setup() {
        util = new Util();
        this.prop = new Properties();
        resourceAsStream = this.getClass().getResourceAsStream("/application.properties");
    }

    @Test
    public void shouldGetServerPort() {
        assertTrue(util.getServerPort() == 8000);
    }

    @Test
    public void shouldGetSessionExpirationMinutes() {
        assertTrue(util.getSessionExpirationMinutes() == 10);
    }

}
