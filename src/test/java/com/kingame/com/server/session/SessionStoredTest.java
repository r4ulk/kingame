package com.kingame.com.server.session;


import com.king.game.server.session.ServerSession;
import com.king.game.server.session.SessionStored;
import com.king.game.server.session.SessionTimer;
import com.king.game.server.session.SessionUser;
import com.king.game.server.session.impl.ServerSessionImpl;
import com.king.game.server.session.impl.SessionTimerImpl;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.concurrent.ConcurrentHashMap;

public class SessionStoredTest {

    private ServerSession serverSession;

    private SessionTimer sessionTimer;

    private SessionUser sessionUser;

    private SessionStored sessionStored;

    private static final String sessionId = "123456";

    private static final Integer userId = 777;

    @Before
    public void setup() {
        sessionStored = new SessionStored();
        sessionUser   = new SessionUser(userId, sessionId);
        sessionTimer  = new SessionTimerImpl();
        serverSession = new ServerSessionImpl(sessionId, sessionTimer.getTimestamp(), sessionUser);
        sessionStored.push(serverSession);
    }

    @Test
    public void shouldPush() {
        ConcurrentHashMap<String, ServerSession> sessions = sessionStored.getAll();
        assertEquals(sessions.size(), 1);
    }

    @Test
    public void shouldGetByUserId() {
        ServerSession ss = sessionStored.getByUserId(userId);
        assertNotNull(ss);
        assertEquals(ss.getSessionId(), sessionId);
    }

    @Test
    public void shouldGetBySessionId() {
        ServerSession ss = sessionStored.getBySessionId(sessionId);
        assertNotNull(ss);
        assertNotNull(ss.getUser());
        assertEquals(ss.getUser().getUserId(), userId);
    }

    @Test
    public void shouldGetAll() {
        assertEquals(sessionStored.getAll().size(), 1);
    }

    @Test
    public void shouldGetHighScore() {
        ServerSession ss = sessionStored.getByUserId(userId);
        ss.getUser().setScore(1, 500);
        assertEquals(sessionStored.getHighscores(1).toString(), "{"+userId+"=500}");
        assertTrue(sessionStored.getHighscores(1).size() <= 15);
        ServerSession ss1 = new ServerSessionImpl("123", sessionTimer.getTimestamp(), new SessionUser(666, "123"));
    }


}
