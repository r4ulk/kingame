package com.kingame.com.server;

import com.king.game.server.SimpleHttpServer;
import com.king.game.server.request.Request;
import com.king.game.server.response.Response;
import com.king.game.utils.Util;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Map;

/**
 * @author Raul Klumpp and a Bit of Google inspiration
 */
public class SimpleHttpServerTest {

    private static final String HOST = "http://localhost:8000/";

    SimpleHttpServer simpleHttpServer;

    @Before
    public void setup() {
        Util util = new Util();
        int serverPort = util.getServerPort();
        simpleHttpServer = new SimpleHttpServer(serverPort, "/");
        simpleHttpServer.start();
    }

    @After
    public void tearDown() {
        simpleHttpServer.stop();
    }

    @Test
    public void shouldRequestLogin(){
        HttpResponse response = request("api/7/login", Request.GET.getDescription(), null, null);
        assertEquals(Response.Status.OK.getStatusCode(), response.getStatusLine().getStatusCode());
        BufferedReader br = null;
        String sessionId = null;
        try {
            br = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
            sessionId = br.readLine();
        } catch (IOException e) {
            e.printStackTrace();
        }
        assertNotNull(sessionId);
    }

    @Test
    public void shouldRequestLoginWithInvalidUserPath(){
        HttpResponse response = request("api//login", Request.GET.getDescription(), null, null);
        assertEquals(Response.Status.BAD_REQUEST.getStatusCode(), response.getStatusLine().getStatusCode());
    }

    @Test
    public void shouldAddScore(){
        HttpResponse response = request("api/7/login", Request.GET.getDescription(), null, null);
        assertEquals(Response.Status.OK.getStatusCode(), response.getStatusLine().getStatusCode());
        BufferedReader br = null;
        String sessionId = null;
        try {
            br = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
            sessionId = br.readLine();
            assertNotNull(sessionId);
            response = request("api/7/score?sessionkey=" + sessionId, Request.POST.getDescription(), "150", null);
            assertEquals(Response.Status.OK.getStatusCode(), response.getStatusLine().getStatusCode());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void shouldTryAddScoreWithInvalidSession(){
        String sessionId = "0000000";
        HttpResponse response = request("api/7/score?sessionkey=" + sessionId, Request.POST.getDescription(), "150", null);
        assertEquals(Response.Status.INTERNAL_SERVER_ERROR.getStatusCode(), response.getStatusLine().getStatusCode());
    }

    /*
     * Google inspiration absolutely
     */
    private static HttpResponse request(String path, String method, String body, Map<String, String> headers)  {
        try {
            HttpClient client =  HttpClientBuilder.create().disableRedirectHandling().build();
            HttpUriRequest request;
            switch (method) {
                case "GET":
                    request = new HttpGet(HOST + path);
                    if (null != headers && !headers.isEmpty())
                        for (Map.Entry<String, String> h : headers.entrySet())
                            request.addHeader(h.getKey(), h.getValue());
                    break;
                case "POST":
                    request = new HttpPost(HOST + path);
                    if (null != body)
                        ((HttpPost) request).setEntity(new ByteArrayEntity(body.getBytes("UTF-8")));
                    if (null != headers && !headers.isEmpty())
                        for (Map.Entry<String, String> h : headers.entrySet())
                            request.addHeader(h.getKey(), h.getValue());
                    break;
                default:
                    throw new RuntimeException("Request not allowed or not found");
            }
            return client.execute(request);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

}
