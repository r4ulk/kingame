
import com.king.game.server.SimpleHttpServer;
import com.king.game.utils.Util;

/**
 * @author Raul Klumpp - raulklumpp@gmail.com
 * Application initializer
 */
public class Application {

    public static void main(String[] args) {
        Util util = new Util();
        int serverPort = util.getServerPort();
        SimpleHttpServer simpleHttpServer = new SimpleHttpServer(serverPort, "/");
        simpleHttpServer.start();
        System.out.println("Server is started and listening on port " + serverPort);
    }



}
