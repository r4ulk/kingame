package com.king.game.server.response;

import com.king.game.server.ServerExchange;

import java.io.IOException;
import java.io.OutputStream;

/**
 * @author Raul Klumpp - raulklumpp@gmail.com
 */
public class Response {

    public static void send(int status, String message, ServerExchange exchange) throws IOException {
        exchange.setStatus(status, message.length());
        OutputStream os = exchange.getResponseBody();
        os.write(message.getBytes("UTF-8"));
    }

    public enum Status {

        ACCEPTED(202, "Accepted"),

        OK(200, "OK"),

        INTERNAL_SERVER_ERROR(500, "Internal Server Error"),

        UNAUTHORIZED(401, "Unauthorized"),

        BAD_REQUEST(400, "Bad Request");

        private final int statusCode;

        private final String description;

        Status(int statusCode, String description) {
            this.statusCode = statusCode;
            this.description = description;
        }

        public int getStatusCode() {
            return statusCode;
        }

        public String getDescription() {
            return description;
        }

    }
}
