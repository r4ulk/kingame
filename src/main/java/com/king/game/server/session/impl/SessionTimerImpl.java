package com.king.game.server.session.impl;

import com.king.game.server.session.SessionTimer;

import java.util.Date;

/**
 * @author Raul Klumpp - raulklumpp@gmail.com
 */
public class SessionTimerImpl implements SessionTimer {

    @Override
    public long getTimestamp() {
        return new Date().getTime();
    }
}
