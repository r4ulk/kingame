package com.king.game.server.session;

import com.king.game.utils.Util;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author Raul Klumpp and a Bit of Google inspiration
 */
public class SessionStored {

    private Util util;

    /*
        ConcurrentHashMap<UserId, ServerSession>
     */
    private final ConcurrentHashMap<String, ServerSession> sessions = new ConcurrentHashMap<>(8, 0.9f, 1);

    public SessionStored() {
        this.util = new Util();
    }

    public void push(ServerSession serverSession) {
        this.sessions.put(serverSession.getUser().getUserId().toString(), serverSession);
    }

    public ServerSession getByUserId(Integer userId) {
        return sessions.get(userId.toString());
    }

    public ServerSession getBySessionId(String sessionId) {
        return sessions.searchValues(1, v -> v.getSessionId().equals(sessionId) ? v : null);
    }

    public Map<Integer, Integer> getHighscores(int levelId) {
        Map<Integer, Integer> userscores = new HashMap<>();
        Map<Integer, Map<Integer, Integer>> highscores = new HashMap<>();
        sessions.forEach((k, v) -> {
            if(v.getUser() != null && v.getUser().getUserId() != null && v.getUser().getScore(levelId) != null) {
                userscores.put(v.getUser().getUserId(), v.getUser().getScore(levelId));
                highscores.put(levelId, userscores);
            }
        });
        if(highscores.get(levelId) != null && !highscores.get(levelId).isEmpty()) {
            int i = 0;
            Map<Integer, Integer> ordered = new LinkedHashMap<>();
            highscores.get(levelId).entrySet().stream()
                    .sorted(Map.Entry.<Integer, Integer>comparingByValue().reversed())
                    .limit(15)
                    .forEachOrdered(x -> ordered.put(x.getKey(), x.getValue()));
            return ordered;

        }
        return null;
    }

    public ConcurrentHashMap<String, ServerSession> getAll() {
        return this.sessions;
    }

}
