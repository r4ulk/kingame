package com.king.game.server.session.impl;

import com.king.game.server.session.ServerSession;
import com.king.game.server.session.SessionTimer;
import com.king.game.server.session.SessionUser;
import com.king.game.utils.Util;

import java.time.Duration;

/**
 * @author Raul Klumpp - raulklumpp@gmail.com
 */
public class ServerSessionImpl implements ServerSession {

    private long expires;

    private final String sessionId;

    private final long created;

    private final SessionUser user;

    public ServerSessionImpl(String sessionId, long created, SessionUser user) {
        Util util = new Util();
        this.sessionId = sessionId;
        this.created = created;
        this.user = user;
        expires = Duration.ofMinutes(util.getSessionExpirationMinutes()).toMillis();
    }

    @Override
    public SessionUser getUser() {
        return user;
    }

    @Override
    public String getSessionId() {
        return sessionId;
    }

    @Override
    public boolean isValid(SessionTimer timer) {
        return timer.getTimestamp() - this.created < expires;
    }

    @Override
    public String toString() {
        return "ServerSession {" +
                "sessionId='" + sessionId + '\'' +
                ", created=" + created +
                ", user=" + user.toString() +
                '}';
    }
}
