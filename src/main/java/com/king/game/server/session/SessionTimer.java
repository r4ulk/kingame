package com.king.game.server.session;

/**
 * @author Raul Klumpp - raulklumpp@gmail.com
 */
public interface SessionTimer {

    long getTimestamp();

}
