package com.king.game.server.session;

/**
 * @author Raul Klumpp - raulklumpp@gmail.com
 */
public interface ServerSession {

    SessionUser getUser();

    String getSessionId();

    boolean isValid(SessionTimer timer);

}
