package com.king.game.server.session;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Raul Klumpp - raulklumpp@gmail.com
 */
public class SessionUser {

    private Integer userId;

    private String sessionId;

    /*
        Map<LevelId, Score>
     */
    private Map<Integer, Integer> score = new HashMap<>();

    public SessionUser(Integer userId, String sessionId) {
        this.userId = userId;
        this.sessionId = sessionId;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getSessionId() {
        return this.sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public Integer getScore(Integer levelId) {
        return this.score.get(levelId);
    }

    public void setScore(Integer levelId, int score) {
        this.score.put(levelId, score);
    }

    @Override
    public String toString() {
        return "SessionUser{" +
                "userId='" + userId + '\'' +
                ", sessionId='" + sessionId + '\'' +
                ", score=" + score +
                '}';
    }
}
