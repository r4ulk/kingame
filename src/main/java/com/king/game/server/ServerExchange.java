package com.king.game.server;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URI;
import java.util.List;
import java.util.Map;

/**
 * @author Raul Klumpp - raulklumpp@gmail.com
 */
public interface ServerExchange {

        String getRequestMethod();

        URI getRequestURI();

        Map<String, List<String>> getRequestHeaders();

        Map<String, List<String>> getResponseHeaders();

        InputStream getRequestBody();

        OutputStream getResponseBody();

        void setStatus(int statusCode, long responseLength) throws IOException;

        void close();

}
