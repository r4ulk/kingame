package com.king.game.server;

import com.king.game.factory.ServiceFactory;
import com.king.game.server.router.Router;
import com.king.game.server.router.ServerAction;
import com.king.game.server.router.impl.RouterImpl;
import com.sun.net.httpserver.HttpServer;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Raul Klumpp - raulklumpp@gmail.com
 */
public class SimpleHttpServer {

    private HttpServer httpServer;

    private final Map<String, ServerAction> routes =
            new HashMap<String, ServerAction>() {{
                put("GET/api/(.+)/login", ServerAction.LOGIN);
                put("POST/api/(.+)/score$", ServerAction.SCORE);
                put("GET/api/(.+)/highscorelist", ServerAction.HIGH_SCORE);
            }};

    public SimpleHttpServer(int port, String context) {
        try {
            httpServer = HttpServer.create(new InetSocketAddress(port), 0);
            Router router = new RouterImpl(routes);
            httpServer.createContext(context, new ServerHandler(new ServiceFactory(router)));
            httpServer.setExecutor(null);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public void start() {
        this.httpServer.start();
    }

    public void stop() {
        this.httpServer.stop(0);
    }

}
