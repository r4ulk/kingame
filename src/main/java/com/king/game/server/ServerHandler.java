package com.king.game.server;

import com.king.game.factory.ServiceFactory;
import com.king.game.server.impl.ServerExchangeImpl;
import com.king.game.service.GameService;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;

import java.io.IOException;

/**
 * @author Raul Klumpp - raulklumpp@gmail.com
 */
public class ServerHandler implements HttpHandler {

    private final ServiceFactory serviceFactory;

    public ServerHandler(ServiceFactory serviceFactory) {
        this.serviceFactory = serviceFactory;
    }

    @Override
    public void handle(HttpExchange httpExchange) throws IOException {
        ServerExchange exchange = new ServerExchangeImpl(httpExchange);
        GameService gameService = serviceFactory.getService(exchange);
        gameService.execute(exchange);
    }

}
