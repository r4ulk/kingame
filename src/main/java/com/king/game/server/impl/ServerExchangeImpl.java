package com.king.game.server.impl;

import com.king.game.server.ServerExchange;
import com.sun.net.httpserver.HttpExchange;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URI;
import java.util.List;
import java.util.Map;

/**
 * @author Raul Klumpp - raulklumpp@gmail.com
 */
public class ServerExchangeImpl implements ServerExchange {

    private final HttpExchange exchange;

    public ServerExchangeImpl(HttpExchange exchange) {
        this.exchange = exchange;
    }

    @Override
    public String getRequestMethod() {
        return exchange.getRequestMethod();
    }

    @Override
    public URI getRequestURI() {
        return exchange.getRequestURI();
    }

    @Override
    public Map<String, List<String>> getRequestHeaders() {
        return exchange.getRequestHeaders();
    }

    @Override
    public Map<String, List<String>> getResponseHeaders() {
        return exchange.getResponseHeaders();
    }

    @Override
    public InputStream getRequestBody() {
        return exchange.getRequestBody();
    }

    @Override
    public OutputStream getResponseBody() {
        return exchange.getResponseBody();
    }

    @Override
    public void setStatus(int statusCode, long responseLength) throws IOException {
        exchange.sendResponseHeaders(statusCode, responseLength);
    }

    @Override
    public void close() {
        exchange.close();
    }

}
