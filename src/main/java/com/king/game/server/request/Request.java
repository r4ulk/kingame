package com.king.game.server.request;

/**
 * @author Raul Klumpp - raulklumpp@gmail.com
 */
public enum Request {

    GET("GET"),

    POST("POST"),

    PUT("PUT"),

    DELETE("DELETE");

    private final String description;

    Request(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }
}
