package com.king.game.server.router;

/**
 * @author Raul Klumpp - raulklumpp@gmail.com
 */
public enum ServerAction {

    LOGIN,
    SCORE,
    HIGH_SCORE

}