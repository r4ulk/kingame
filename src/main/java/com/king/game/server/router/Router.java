package com.king.game.server.router;

import com.king.game.server.ServerExchange;

import java.util.Optional;

/**
 * @author Raul Klumpp - raulklumpp@gmail.com
 */
public interface Router {

    Optional<ServerAction> get(ServerExchange exchange);

}
