package com.king.game.server.router.impl;

import com.king.game.server.ServerExchange;
import com.king.game.server.router.Router;
import com.king.game.server.router.ServerAction;

import java.util.Map;
import java.util.Optional;
import java.util.regex.Pattern;

/**
 * @author Raul Klumpp - raulklumpp@gmail.com
 */
public class RouterImpl implements Router {

    private final Map<String, ServerAction> router;

    public RouterImpl(Map<String, ServerAction> router) {
        this.router = router;
    }

    @Override
    public Optional<ServerAction> get(ServerExchange exchange) {
        for (Map.Entry<String, ServerAction> e: router.entrySet()) {
            if (Pattern.matches(e.getKey(), setRouterKey(exchange)))
                return Optional.of(e.getValue());
        }
        return Optional.empty();
    }

    private String setRouterKey(ServerExchange exchange) {
        return exchange.getRequestMethod() + exchange.getRequestURI().getPath();
    }

}
