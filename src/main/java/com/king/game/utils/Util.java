package com.king.game.utils;

import com.king.game.server.ServerExchange;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Map;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author Raul Klumpp - raulklumpp@gmail.com
 * Helper utilities
 */
public class Util {

    private Properties prop;

    InputStream resourceAsStream;

    public Util() {
        this.prop = new Properties();
        resourceAsStream = this.getClass().getResourceAsStream("/application.properties");
    }

    public int getServerPort() {
        try {
            prop.load(resourceAsStream);
            return Integer.parseInt(prop.getProperty("server.port"));
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return 0;
    }

    public long getSessionExpirationMinutes() {
        try {
            prop.load(resourceAsStream);
            return Integer.parseInt(prop.getProperty("session.expires.minutes"));
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return 0;
    }

    public String getPathParam(ServerExchange exchange, Pattern pattern) {
        Matcher matcher = pattern.matcher(exchange.getRequestURI().getPath());
        if (matcher.find()) return matcher.group(1);
        return null;
    }

    public String getQueryParam(ServerExchange exchange, Pattern pattern) {
        String paramValue = null;
        String queryParams = exchange.getRequestURI().getQuery();
        loop :
        for(String param : queryParams.split("&") ) {
            Matcher matcher = pattern.matcher(param);
            if (matcher.find()) paramValue = matcher.group(1);
            break loop;
        }
        return paramValue;
    }

    public String getRequestBodyContent(ServerExchange exchange) {
        String reqBody = null;
        try {
            BufferedReader br = new BufferedReader(new InputStreamReader(exchange.getRequestBody()));
            reqBody = br.readLine();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return reqBody;
    }

    public String getHighscoreToString(Map<Integer, Integer> highscore) {
        return highscore.toString()
                .replace("{", "")
                .replace("}", "")
                .replace(" ", "");
    }

}
