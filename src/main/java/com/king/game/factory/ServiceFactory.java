package com.king.game.factory;

import com.king.game.server.ServerExchange;
import com.king.game.server.router.Router;
import com.king.game.server.session.SessionStored;
import com.king.game.service.GameService;
import com.king.game.service.impl.HighScoreServiceImpl;
import com.king.game.service.impl.LoginServiceImpl;
import com.king.game.service.impl.NotFoundService;
import com.king.game.service.impl.ScoreServiceImpl;

/**
 * @author Raul Klumpp and a Bit of Google inspiration
 */
public class ServiceFactory {

    private final Router router;

    private final GameService login, score, highscore, notfound;

    private SessionStored sessionStored;

    public ServiceFactory(Router router) {
        this.router = router;
        this.sessionStored = new SessionStored();
        this.login = new LoginServiceImpl(  sessionStored);
        this.score = new ScoreServiceImpl(sessionStored);
        this.highscore = new HighScoreServiceImpl(sessionStored);
        this.notfound = new NotFoundService();
    }

    public GameService getService(ServerExchange exchange) {
        return router
                .get(exchange)
                .map(serverAction -> {
                    GameService service;
                    switch (serverAction) {
                        case LOGIN:
                            service = login;
                            break;
                        case SCORE:
                            service = score;
                            break;
                        case HIGH_SCORE:
                            service = highscore;
                            break;
                        default:
                            service = notfound;
                            break;
                    }
                    return service;
                })
                .orElse(new NotFoundService());
    }

}
