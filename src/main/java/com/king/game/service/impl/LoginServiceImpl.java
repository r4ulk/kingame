package com.king.game.service.impl;

import com.king.game.server.request.Request;
import com.king.game.server.ServerExchange;
import com.king.game.server.response.Response;
import com.king.game.server.session.ServerSession;
import com.king.game.server.session.SessionStored;
import com.king.game.server.session.SessionTimer;
import com.king.game.server.session.impl.ServerSessionImpl;
import com.king.game.server.session.impl.SessionTimerImpl;
import com.king.game.server.session.SessionUser;
import com.king.game.service.GameService;
import com.king.game.utils.Util;

import java.io.IOException;
import java.util.UUID;
import java.util.regex.Pattern;

/**
 * @author Raul Klumpp - raulklumpp@gmail.com
 */
public class LoginServiceImpl implements GameService {

    private static final Pattern loginPattern = Pattern.compile("/api/(.+)/login/?");

    private ServerSession serverSession;

    private SessionStored sessionStored;

    private SessionTimer sessionTimer;

    private long timestamp;

    private Util util;

    public LoginServiceImpl(SessionStored sessionStored) {
        this.util = new Util();
        this.sessionStored  = sessionStored;
        this.sessionTimer   = new SessionTimerImpl();
    }

    @Override
    public void execute(ServerExchange exchange) throws IOException {
        if(exchange.getRequestMethod().equals(Request.GET.getDescription())) {
            login(exchange);
        }
    }

    private void login(ServerExchange exchange) throws IOException {
        String userId = util.getPathParam(exchange, loginPattern);
        String sessionId = null;
        if(userId != null && !userId.isEmpty()) {
            ServerSession ss = sessionStored.getByUserId(Integer.parseInt(userId));
            if(ss != null){
                if(ss.isValid(sessionTimer)) {
                    sessionId = ss.getSessionId();
                    Response.send(Response.Status.OK.getStatusCode(), sessionId.toString(), exchange);
                }else {
                    Response.send(Response.Status.UNAUTHORIZED.getStatusCode(), "Error: " +
                            Response.Status.UNAUTHORIZED.getDescription(), exchange);
                }
            }else {
                this.timestamp = sessionTimer.getTimestamp();
                sessionId = UUID.randomUUID().toString().replace("-", "");
                serverSession = new ServerSessionImpl(sessionId, this.timestamp, new SessionUser(Integer.parseInt(userId), sessionId));
                sessionStored.push(serverSession);
                Response.send(Response.Status.OK.getStatusCode(), sessionId.toString(), exchange);
            }
        }
        Response.send(Response.Status.INTERNAL_SERVER_ERROR.getStatusCode(), "Error: " +
                Response.Status.INTERNAL_SERVER_ERROR.getDescription(), exchange);

    }

}
