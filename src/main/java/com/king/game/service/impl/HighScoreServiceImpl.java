package com.king.game.service.impl;

import com.king.game.server.ServerExchange;
import com.king.game.server.request.Request;
import com.king.game.server.response.Response;
import com.king.game.server.session.SessionStored;
import com.king.game.server.session.SessionTimer;
import com.king.game.server.session.impl.SessionTimerImpl;
import com.king.game.service.GameService;
import com.king.game.utils.Util;

import java.io.IOException;
import java.util.Map;
import java.util.regex.Pattern;

/**
 * @author Raul Klumpp - raulklumpp@gmail.com
 */
public class HighScoreServiceImpl implements GameService {

    private static final Pattern levelIdPattern = Pattern.compile("/api/(.+)/highscorelist?");

    private SessionStored sessionStored;

    private SessionTimer sessionTimer;

    private Util util;

    public HighScoreServiceImpl(SessionStored sessionStored) {
        this.util = new Util();
        this.sessionStored  = sessionStored;
        this.sessionTimer   = new SessionTimerImpl();
    }

    @Override
    public void execute(ServerExchange exchange) throws IOException {
        if(exchange.getRequestMethod().equals(Request.GET.getDescription())) {
            highscore(exchange);
        }
    }

    private void highscore(ServerExchange exchange) throws IOException {
        String levelId = util.getPathParam(exchange, levelIdPattern);
        Map<Integer, Integer> highscore = sessionStored.getHighscores(Integer.parseInt(levelId));
        Response.send(Response.Status.OK.getStatusCode(), (highscore != null) ? util.getHighscoreToString(highscore) : " ", exchange);
    }

}
