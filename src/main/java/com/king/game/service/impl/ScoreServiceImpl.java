package com.king.game.service.impl;

import com.king.game.server.ServerExchange;
import com.king.game.server.request.Request;
import com.king.game.server.response.Response;
import com.king.game.server.session.ServerSession;
import com.king.game.server.session.SessionStored;
import com.king.game.server.session.SessionTimer;
import com.king.game.server.session.impl.SessionTimerImpl;
import com.king.game.service.GameService;
import com.king.game.utils.Util;

import java.io.IOException;
import java.util.regex.Pattern;

/**
 * @author Raul Klumpp - raulklumpp@gmail.com
 */
public class ScoreServiceImpl implements GameService {

    private static final Pattern levelIdPattern = Pattern.compile("/api/(.+)/score?");

    private static final Pattern sessionKeyPattern = Pattern.compile("sessionkey=(.+)?");

    private SessionStored sessionStored;

    private SessionTimer sessionTimer;

    private Util util;

    public ScoreServiceImpl(SessionStored sessionStored) {
        this.util = new Util();
        this.sessionStored  = sessionStored;
        this.sessionTimer   = new SessionTimerImpl();
    }

    @Override
    public void execute(ServerExchange exchange) throws IOException {
        if(exchange.getRequestMethod().equals(Request.POST.getDescription())) {
            score(exchange);
        }
    }

    private void score(ServerExchange exchange) throws IOException {
        String levelId   = util.getPathParam(exchange, levelIdPattern);
        String sessionId = util.getQueryParam(exchange,  sessionKeyPattern);
        String score = util.getRequestBodyContent(exchange);
        if(levelId == null || sessionId == null || score == null ) {
            Response.send(Response.Status.BAD_REQUEST.getStatusCode(), "Error: " +
                    Response.Status.BAD_REQUEST.getDescription(), exchange);
        }
        ServerSession ss = sessionStored.getBySessionId(sessionId);
        if(ss != null) {
            if(ss.isValid(sessionTimer)){
                ss.getUser().setScore(Integer.parseInt(levelId),  Integer.parseInt(score));
                Response.send(Response.Status.OK.getStatusCode(), " ", exchange);
            }
        }
        Response.send(Response.Status.INTERNAL_SERVER_ERROR.getStatusCode(), "Error: " +
                Response.Status.INTERNAL_SERVER_ERROR.getDescription(), exchange);

    }

}
