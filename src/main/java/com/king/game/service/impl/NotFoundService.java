package com.king.game.service.impl;

import com.king.game.server.ServerExchange;
import com.king.game.server.response.Response;
import com.king.game.service.GameService;

import java.io.IOException;

/**
 * @author Raul Klumpp - raulklumpp@gmail.com
 */
public class NotFoundService implements GameService {

    @Override
    public void execute(ServerExchange exchange) throws IOException {
        Response.send(Response.Status.BAD_REQUEST.getStatusCode(), "Error: " +
                Response.Status.BAD_REQUEST.getDescription(), exchange);
    }
}
