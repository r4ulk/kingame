package com.king.game.service;

import com.king.game.server.ServerExchange;

import java.io.IOException;

/**
 * @author Raul Klumpp - raulklumpp@gmail.com
 */
public interface GameService {

    void execute(ServerExchange exchange) throws IOException;

}
