# README #

This a test challenge King using simple httpserver.

I used gradle for manage dependencies.

### How do I get set up? ###

* Run the code in your terminal to Build and Run Tests:
```
./gradlew build
```

### Running ###
```
./gradlew run
```

### Lets try ###

API Login
`GET - http://localhost:8000/api/<userId>/login`

API User score to a level 
```
POST - http://localhost:8000/api/<levelId>/score?sessionkey=<sessionKey>
REQUEST BODY - <score>
```

API Get high score of level 
`GET - http://localhost:8000/api/<levelId>/highscorelist`

### Credits ###

Raul Klumpp <raulklumpp@gmail.com> and a bit of Google inspiration ;\)